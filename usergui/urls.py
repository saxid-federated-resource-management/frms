# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import re_path
from usergui import views

urlpatterns = [
    re_path('^$', views.index, name='index'),
    re_path('^detail/(?P<uuid>[^/]+)/$', views.detail, name="detail"),
]
app_name = 'usergui'
