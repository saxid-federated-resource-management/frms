from django.shortcuts import render, get_object_or_404

from api.models import Resource

# Create your views here.

def index(request):
    resources = Resource.objects.filter(eppn=request.eppn).order_by("sp__name",)
    return render(request, "usergui/index.html", {'resources': resources})

def detail(request, uuid):
    resource = get_object_or_404(Resource, eppn=request.eppn, uuid=uuid)
    return render(request, "usergui/detail.html", {'resource': resource})
