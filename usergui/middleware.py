# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import getpass
import os

class RemoteUserEppnMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    """
    Stores the username from REMOTE_USER in request.eppn.
    """
    def __call__(self, request):
        if "REMOTE_USER" in request.META:
            request.eppn = request.META["REMOTE_USER"]
        else:
            request.eppn = None
        response = self.get_response(request)
        return response

class DevelopmentUserEppnMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
    """
    Stores the username of the current user in request.eppn
    """
    def __call__(self, request):
        if "LOGNAME" in os.environ:
            logname = os.environ["LOGNAME"]
            request.eppn = logname
        else:
            request.eppn = "{}@tu-chemnitz.de".format(getpass.getuser())
        response = self.get_response(request)
        return response
