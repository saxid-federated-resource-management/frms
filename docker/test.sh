#!/usr/bin/env bash
docker-compose up -d
sleep 30
python3 tests/test.py
success=$?
docker-compose down
exit $success
