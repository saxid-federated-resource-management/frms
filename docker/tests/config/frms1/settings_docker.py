# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

SERVER_REALM="frms1.edu"     # hs-mittweida.de
HTTP_SERVER_NAME="http://frms1-web" # https://saxid-api.hs-mittweida.de
ALLOWED_HOSTS = ["*"]   # saxid-api.hs-mittweida.de

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'frms1',
        'USER': 'frms1',
        'PASSWORD': 'frms1', # @UndefinedVariable
        'HOST': 'db1',
    }

}
