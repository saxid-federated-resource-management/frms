#!/usr/bin/env bash

FRMS_DIR=/var/www/django/frms

if [[ ! -f $FRMS_DIR/private/settings_docker.py ]]; then
cat <<EOF

settings_docker.py fehlt im Volume, das in /var/www/django/frms/private 
gemountet wird. Darin müssen die Hochschulspezifischen Einstellungen gemacht 
werden.

SERVER_REALM="..."     # hs-mittweida.de
HTTP_SERVER_NAME="..." # https://saxid-api.hs-mittweida.de
ALLOWED_HOSTS = [""]   # saxid-api.hs-mittweida.de

DATABASES = {
}

EOF
exit
fi

sleep 5
chown -R www-data:www-data "${FRMS_DIR}/logs"
su www-data -s /bin/bash -c "${FRMS_DIR}/env/bin/python ${FRMS_DIR}/app/manage.py migrate"
su www-data -s /bin/bash -c "${FRMS_DIR}/env/bin/python ${FRMS_DIR}/app/manage.py collectstatic --noinput"
${FRMS_DIR}/env/bin/python ${FRMS_DIR}/app/manage.py loaddata $FRMS_DIR/private/fixtures-1.json
echo "starte uwsgi"
/usr/bin/uwsgi --ini /etc/uwsgi/apps-enabled/frms.ini
