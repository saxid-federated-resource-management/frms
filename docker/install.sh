#!/usr/bin/env bash
TARGET=/var/www/django/frms
mkdir -p /var/www/django/frms/logs
git clone https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms.git $TARGET/app
cd $TARGET/app
#git checkout django22_vektorfix
python3 -mvenv $TARGET/env
source $TARGET/env/bin/activate
cd $TARGET/app
pip install -r requirements.txt
pip install ipython
pip install mysqlclient

mkdir -p $TARGET/static
chown www-data: $TARGET/static
