# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.auth.models
import django.utils.timezone
import django.core.validators
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='TokenAuthUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, max_length=30, validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.', 'invalid')], help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, verbose_name='username')),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(max_length=254, verbose_name='email address', blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('user_type', models.CharField(max_length=5, verbose_name='User type', choices=[('api', 'API user'), ('idp', 'IdP user'), ('sp', 'SP user'), ('sync', 'Sync user')])),
                ('realm', models.CharField(help_text='Must be set for type SYNC!', max_length=100, verbose_name='Realm', blank=True)),
                ('api_url', models.CharField(help_text='Must be set for type SYNC!', max_length=100, verbose_name='API URL', blank=True)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, db_index=True)),
                ('default_value', models.CharField(max_length=100)),
                ('value', models.CharField(max_length=100)),
                ('vec_sp', models.IntegerField(default=1, verbose_name='VV (SP)')),
                ('vec_idp', models.IntegerField(default=0, verbose_name='VV (IdP)')),
                ('prio', models.CharField(default='sp', max_length=5, verbose_name='Priority role', choices=[('idp', 'Identity Provider'), ('sp', 'Service Provider')])),
            ],
        ),
        migrations.CreateModel(
            name='QueueJob',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=5, verbose_name='Job type', choices=[('att', 'Attribute'), ('del', 'Deletion'), ('res', 'Resource')])),
                ('obj', models.TextField(verbose_name='JSON-serialized object', null=True)),
                ('status', models.CharField(default='rdy', max_length=5, verbose_name='Job status', choices=[('fsh', 'Finished'), ('rdy', 'Ready')])),
                ('realm', models.CharField(max_length=100, verbose_name='Destination realm')),
                ('res_uuid', models.UUIDField(null=True, verbose_name='Resource UUID', blank=True)),
                ('added', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Queue Job',
            },
        ),
        migrations.CreateModel(
            name='Resource',
            fields=[
                ('uuid', models.UUIDField(primary_key=True, default=uuid.uuid4, serialize=False, unique=True, verbose_name='UUID')),
                ('setup_token', models.TextField()),
                ('eppn', models.CharField(max_length=100, verbose_name=' ePPN', db_index=True)),
                ('realm_idp', models.CharField(verbose_name='Realm (IdP)', max_length=100, editable=False)),
                ('added', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('expiry_date', models.DateTimeField(db_index=True)),
                ('deletion_date', models.DateTimeField(db_index=True)),
                ('sp_primary_key', models.CharField(max_length=255, verbose_name='Primary Key of resource in SP software', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ServiceProvider',
            fields=[
                ('uuid', models.UUIDField(primary_key=True, default=uuid.uuid4, serialize=False, unique=True, verbose_name='UUID')),
                ('name', models.CharField(max_length=100)),
                ('realm', models.CharField(max_length=100, verbose_name='Realm')),
                ('expiry_date', models.DateTimeField()),
            ],
            options={
                'verbose_name': 'Service Provider',
            },
        ),
        migrations.AddField(
            model_name='resource',
            name='sp',
            field=models.ForeignKey(verbose_name='SP', to='api.ServiceProvider', on_delete=models.PROTECT),
        ),
        migrations.AddField(
            model_name='attribute',
            name='resource',
            field=models.ForeignKey(related_name='attributes', to='api.Resource', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='tokenauthuser',
            name='sp',
            field=models.ForeignKey(blank=True, to='api.ServiceProvider', help_text='Must be set for type SP!', null=True, verbose_name='SP', on_delete=models.PROTECT),
        ),
        migrations.AddField(
            model_name='tokenauthuser',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
        migrations.AlterUniqueTogether(
            name='attribute',
            unique_together=set([('name', 'resource')]),
        ),
    ]
