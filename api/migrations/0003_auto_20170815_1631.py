# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20161215_1151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='localresourcepolicy',
            name='delete_after_days',
            field=models.IntegerField(default=90, help_text='keep the resource this number of days in the real world after the user account has expired. This value may be overridden by a local ResourcePolicy.', verbose_name='Default delete policy for resources'),
        ),
        migrations.AlterField(
            model_name='localresourcepolicy',
            name='purge_api_data_after_days',
            field=models.IntegerField(default=7, help_text='purge the resource record this number of days in the database after the resource has been deleted in the real world', verbose_name='Default Purge policy for resource records'),
        ),
    ]
