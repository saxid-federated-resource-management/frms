#   This file is part of frms.
#
#   frms is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Foobar is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

# coding=utf-8
import requests

from django.conf import settings
from django.db import transaction
from io import BytesIO
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import PermissionDenied, ParseError
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Resource, ServiceProvider, Attribute, UserType
from api.serializers import ServiceSerializer, ResourceSerializer, AttributeSerializer
from api.signals import resource_created
from api.utils.csv_handling import sync_from_csv_data
from api.utils.queue_utils import dispatch_resource_sync, dispatch_deletion_sync
from api.utils.user_utils import local_reverse
from api.utils.validation_utils import validate_uuid4



class AuthAPIView(APIView):
    """
    Overrides APIView in order to require token authentication.
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)


class Health(AuthAPIView):
    def get(self, request):
        """
        Resource to test API endpoint.
        """
        try:
            user = Token.objects.get(key=request.auth).user
            return Response(data=str(user), status=200)
        except Resource.DoesNotExist:
            return Response(status=401)


class ServiceList(AuthAPIView):
    def get(self, request):
        """
        List all services that are available to foreign institutes.
        """
        services = ServiceProvider.objects.local()
        ser = ServiceSerializer(services, many=True)
        return Response(ser.data)


class ServiceDetail(AuthAPIView):
    def get(self, request, uuid):
        """
        Detail a specific service that is available to foreign institutes.
        """
        try:
            service = ServiceProvider.objects.local().get(uuid=uuid)
        except ServiceProvider.DoesNotExist:
            return Response(status=404)

        ser = ServiceSerializer(service)
        return Response(ser.data)


class ResourceList(AuthAPIView):
    def get(self, request):
        if request.user.user_type in (UserType.TYPE_API, UserType.TYPE_IDP):
            resources = Resource.objects.filter(realm_idp=settings.SERVER_REALM)
        elif request.user.user_type == UserType.TYPE_SP:
            resources = Resource.objects.filter(sp=request.user.sp)
        else:
            return Response(status=404)

        ser = ResourceSerializer(resources, many=True)
        return Response(ser.data)

    def post(self, request):
        """
        Create a new local resource entry. Based on the token,
        decide whether it's a local API/SP request or from a remote institute
        in order to synchronize.
        """
        if request.data and request.data["sp"]:
            uuid = request.data.get("sp", "")
        else:
            raise ParseError("No SP field provided.")

        if not validate_uuid4(uuid):
            raise ParseError("Specified SP is not a valid UUID.")

        if request.user.user_type in UserType.TYPE_SP:
            sp_primary_key = request.data.get("sp_primary_key", "")
            if not ServiceProvider.objects.get(uuid=uuid):
                return Response(status=status.HTTP_404_NOT_FOUND)

            if not request.user.sp == ServiceProvider.objects.get(uuid=uuid):
                raise PermissionDenied("Specified SP does not match yours.")
            # prevent double creation of the same resource, if the original
            # application cannot handle this in a clean way (such as nextcloud)
            if sp_primary_key:
                try:
                    obj = Resource.objects.get(sp=request.user.sp, sp_primary_key=sp_primary_key)
                    location = local_reverse("api:resource_detail", args=[str(obj.uuid)])
                    return Response(status=status.HTTP_201_CREATED, headers={"Location": location})
                except Resource.DoesNotExist:
                    pass

        ser = ResourceSerializer(data=request.data)
        ser.is_valid()
        try:
            with transaction.atomic():
                if request.user.user_type == UserType.TYPE_SYNC:
                    # Remote/Sync request
                    if ser.errors.get("sp", False):
                        # Request probably references non-existent SP
                        if not ServiceProvider.objects.filter(uuid=request.data["sp"]).exists():
                            # That is the case, thus fetching that SP entry is now a priority
                            foreign_api_url = request.user\
                                    .remote_reverse('api:service_detail', args=[request.data["sp"]])
                            headers = {'Authorization': 'token ' + str(request.auth)}
                            r = requests.get(foreign_api_url, headers=headers, timeout=5)

                            if r.status_code == status.HTTP_200_OK:
                                # Deserialize JSON string
                                stream = BytesIO(r.content)
                                data = JSONParser().parse(stream)

                                new_sp = ServiceSerializer(data=data)

                                if new_sp.is_valid():
                                    new_sp.save()

                    # Having fetched the potentially missing SP beforehand,
                    # serialization should not throw an error anymore
                    ser = ResourceSerializer(data=request.data)

                if ser.is_valid():
                    obj = ser.save()
                    resource_created.send(sender=self.__class__, resource=obj, user_type=request.user.user_type)

                    # Check if attributes are present
                    if "attributes" in request.data:
                        ser = AttributeSerializer(data=request.data.get("attributes"), many=True)

                        if ser.is_valid(raise_exception=True):
                            ser.save(resource=obj)

                    if not request.user.user_type == UserType.TYPE_SYNC:
                        # Local request
                        if not obj.realm_idp == settings.SERVER_REALM:
                            # Dispatch resource sync procedure in queue
                            dispatch_resource_sync(obj)

                    location = local_reverse("api:resource_detail", args=[str(obj.uuid)])
                    return Response(status=status.HTTP_201_CREATED, headers={"Location": location})
                return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        except requests.exceptions.Timeout:
            return Response(status=status.HTTP_408_REQUEST_TIMEOUT)
        except requests.exceptions.RequestException:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ResourceDetail(AuthAPIView):
    def get(self, request, uuid):
        try:
            resource = Resource.objects.get(uuid=uuid)
        except Resource.DoesNotExist:
            return Response(status=404)

        serializer = ResourceSerializer(resource)
        return Response(serializer.data)

    def delete(self, request, uuid):
        try:
            resource = Resource.objects.get(uuid=uuid)
        except Resource.DoesNotExist:
            return Response(status=404)

        # If any remote system is involved (either SP is remote or the user
        # belongs to a different realm), we must notify the remote system
        # notification is only for notifying a remote FRMS instance, not a SP
        # system
        notify_remote_system = False
        if request.user.user_type == UserType.TYPE_IDP and not resource.sp.is_local:
            notify_remote_system = True
        if request.user.user_type == UserType.TYPE_SP and not resource.owner_managed_by_local_server:
            notify_remote_system = True
        if request.user.user_type == UserType.TYPE_API:
            if not resource.sp.is_local or not resource.owner_managed_by_local_server:
                notify_remote_system = True

        with transaction.atomic():
            resource.delete()
            if notify_remote_system:
                dispatch_deletion_sync(resource, uuid)
            return Response(status=status.HTTP_200_OK)


class ResourceAttributes(AuthAPIView):
    def get(self, request, uuid):
        try:
            resource = Resource.objects.get(uuid=uuid)
            attributes = Attribute.objects.filter(resource=resource)
        except Resource.DoesNotExist:
            return Response(status=404)

        serializer = AttributeSerializer(attributes, many=True)

        dates = [
            {
                "name": "expiry_date",
                "value": resource.expiry_date
            },
            {
                "name": "deletion_date",
                "value": resource.deletion_date
            }
        ]

        return Response(dates + serializer.data)

    def put(self, request, uuid):
        AttributeSerializer(data=request.data, many=True).is_valid(raise_exception=True)

        try:
            resource = Resource.objects.get(uuid=uuid)
        except Resource.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # Flatten list of dicts
        # rmt_attribs = {}
        # for attrib in request.data:
        #     rmt_attribs[attrib["name"]] = attrib[attrib["value"]]

        res = resource.update_attribute(request.data, request.user.user_type)

        location = local_reverse("api:resource_attributes", args=[str(uuid)])
        return Response(data=res, status=status.HTTP_200_OK, headers={"Location": location})


class WelcomeView(APIView):
    def get(self, request):
        return Response(data="FRMS-API-0.1")


class CsvUploadView(AuthAPIView):
    parser_classes = [MultiPartParser]

    def post(self, request):
        if request.user.user_type != UserType.TYPE_API:
            raise PermissionDenied()
        csv_file = request.data.get('csv', None)
        if not csv_file:
            return Response("csv parameter required", status=Response.HTTP_400_BAD_REQUEST)
        date_format = request.data.get('date_format', '%d.%m.%Y')
        sync_from_csv_data(csv_file, date_format)
        return Response('ok')
