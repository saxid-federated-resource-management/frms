#   This file is part of frms.
#
#   frms is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Foobar is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import requests

from django.contrib import admin
from django.contrib import messages
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token

from api.models import ServiceProvider, Resource, Attribute, TokenAuthUser,\
    UserType, QueueJob, LocalResourcePolicy


class TokenAuthUserAdmin(admin.ModelAdmin):
    actions = ['check_connection']

    def check_connection(self, request, queryset):
        for user in queryset:
            token = Token.objects.get(user=user)
            headers = {'Authorization': 'token {}'.format(token)}

            if not user.user_type == UserType.TYPE_SYNC:
                continue

            if not user.api_url:
                messages.warning(request,
                                 "{}: No API URL set".format(user.username))
                continue

            try:
                url = user.remote_reverse("api:health")
                r = requests.get(url, headers=headers, timeout=5)

                if r.status_code == 200:
                    messages.info(request,
                                  "{}: Connection to {} ok".format(user.username, user.api_url))
                else:
                    messages.error(request,
                                   "{}: Authentication with failed".format(user.username))
            except requests.exceptions.ConnectionError:
                messages.error(request,
                               "{}: Connection to {} failed".format(user.username, user.api_url))

    check_connection.short_description = "Check SYNC connection"


admin.site.register(ServiceProvider)


class ResourceAdmin(admin.ModelAdmin):
    list_display = ("eppn", "sp", "expiry_date", "added")
    readonly_fields = ("added", "updated")
    search_fields = ('eppn', "expiry_date")
    list_filter = ("sp__name", "realm_idp")
    ordering = ("-added",)
admin.site.register(Resource, ResourceAdmin)


class AttributeAdmin(admin.ModelAdmin):
    list_display = ("name", "value", "resource", "prio")
admin.site.register(Attribute, AttributeAdmin)

admin.site.register(TokenAuthUser, TokenAuthUserAdmin)


class QueueJobAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    list_display = ("type", "status", "realm", "added")
    list_filter = ("realm", "status", "type")
    search_fields = ("obj", "realm", "res_uuid")
    readonly_fields = ("added",)
    ordering = ("-added",)

admin.site.register(QueueJob, QueueJobAdmin)
admin.site.register(LocalResourcePolicy)

admin.site.unregister(Group)
