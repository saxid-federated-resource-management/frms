from django.core.management import BaseCommand
from api.models import Resource, Attribute, TokenAuthUser, UserType
from api.serializers import ResourceSerializer

import json
import requests
from rest_framework.renderers import JSONRenderer
from datetime import datetime

class Command(BaseCommand):
    def handle(self, *args, **options):
        usermap = {}
        for u in TokenAuthUser.objects.filter(user_type=UserType.TYPE_SYNC):
            usermap[u.realm] = u
        for res in Resource.objects.filter(realm_idp__in=[u.realm for u in usermap.values()]):
            remote_user = usermap[res.realm_idp]
            headers = {'Authorization': 'token ' + remote_user.token()}
            url = remote_user.remote_reverse('api:resource_detail', args=(res.uuid,))
            req = requests.get(url, headers=headers)
            if req.status_code != 200:
                print(f"res {res.uuid} failed: {req.content}")
                continue
            serializer = ResourceSerializer(res)
            ser_data = json.loads(JSONRenderer().render(serializer.data))
            # added und updated sind lokale Zeitstempel, wann die Objekte lokal angelegt wurden
            ser_data.pop('added', None)
            ser_data.pop('updated', None)
            # Zeitformat ist beliebig
            try:
                ser_data['expiry_date'] = datetime.strptime(ser_data['expiry_date'], "%Y-%m-%dT%H:%M:%S%z")
            except ValueError:
                ser_data['expiry_date'] = datetime.strptime(ser_data['expiry_date'], "%Y-%m-%dT%H:%M:%S.%f%z")
            try:
                ser_data['deletion_date'] = datetime.strptime(ser_data['deletion_date'], "%Y-%m-%dT%H:%M:%S%z")
            except ValueError:
                ser_data['deletion_date'] = datetime.strptime(ser_data['deletion_date'], "%Y-%m-%dT%H:%M:%S.%f%z")
            # die Attribute sind in irgendeiner Sortierung -> normieren
            ser_data['attributes'].sort(key=lambda x: x['name'])
            req_data = req.json()
            req_data.pop('added', None)
            req_data.pop('updated', None)
            try:
                req_data['expiry_date'] = datetime.strptime(req_data['expiry_date'], "%Y-%m-%dT%H:%M:%S%z")
            except ValueError:
                req_data['expiry_date'] = datetime.strptime(req_data['expiry_date'], "%Y-%m-%dT%H:%M:%S.%f%z")
            try:
                req_data['deletion_date'] = datetime.strptime(req_data['deletion_date'], "%Y-%m-%dT%H:%M:%S%z")
            except ValueError:
                req_data['deletion_date'] = datetime.strptime(req_data['deletion_date'], "%Y-%m-%dT%H:%M:%S.%f%z")
            # die Attribute sind in irgendeiner Sortierung
            req_data['attributes'].sort(key=lambda x: x['name'])
            if ser_data ==  req_data:
                print(f"res {res.uuid} matches")
            else:
                print(f"res {res.uuid} does not match:\n{ser_data}\n!=\n{req_data}")


