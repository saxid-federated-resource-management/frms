# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management import BaseCommand

from api.models import Resource


class Command(BaseCommand):
    def handle(self, *args, **options):
        res = Resource.objects.get(uuid="6498e83733954472b556b241c5e14d6a")
        res.update_attribute([{"name": "test", "value": 42}])

        # res.update_attribute({"test": 42})
