# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.management import BaseCommand
from django.utils import timezone

from api.models import ServiceProvider, TokenAuthUser, UserType


class Command(BaseCommand):
    help = "creates a service provider and a matching user in the database"

    def add_arguments(self, parser):
        parser.add_argument('name', help="Name of the service provider")

    def handle(self, *args, **options):
        name = options['name']
        sp = ServiceProvider.objects.create(name=name,
                                            expiry_date=timezone.now() + relativedelta(years=1),
                                            realm=settings.SERVER_REALM)
        sp.save()

        sp_user = TokenAuthUser.objects.create_user(username="sp_{}".format(sp.name),
                                                    email=None,
                                                    password=None,
                                                    user_type=UserType.TYPE_SP,
                                                    sp=sp)
        sp_user.save()

        self.stdout.write("SP-UUID: {}".format(sp.uuid))
        self.stdout.write("Token for Service Provider: {}".format(sp_user.token()))
