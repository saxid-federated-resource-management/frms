from __future__ import unicode_literals

from datetime import date, timedelta, datetime

from django.conf import settings
from django.core.management import BaseCommand
from django.db import transaction
from django.utils import timezone
import requests
from rest_framework.reverse import reverse
from urllib.parse import urljoin

from api.models import Resource, UserType, TokenAuthUser
from api.utils.queue_utils import dispatch_deletion_sync


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("uuid", nargs="+")

    @transaction.atomic()
    def handle(self, *args, **options):
        for uuid in options['uuid']:
            res = Resource.objects.get(uuid=uuid)
            remote_user = TokenAuthUser.objects.get(realm=res.realm_idp, user_type=UserType.TYPE_SYNC)
            headers = {'Authorization': 'token ' + remote_user.token()}
            url = remote_user.remote_reverse('api:resource_detail', args=(uuid,))
            req = requests.get(url, headers=headers)
            print(req.content)

