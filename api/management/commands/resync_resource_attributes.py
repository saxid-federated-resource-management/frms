from django.core.management import BaseCommand
from api.models import Resource, Attribute, TokenAuthUser, UserType
from api.utils.queue_utils import dispatch_attributes_sync

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("uuid", nargs="+")

    def handle(self, *args, **options):
        for uuid in options['uuid']:
            res = Resource.objects.get(uuid=uuid)
            attributes = list(res.attributes.all())
            dispatch_attributes_sync(attributes, uuid, [])
