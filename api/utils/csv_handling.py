#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from datetime import timedelta, datetime

from django.conf import settings
from django.db import transaction
from django.utils import timezone

from api.models import Resource, UserType
from api.utils.queue_utils import dispatch_deletion_sync


@transaction.atomic()
def sync_from_csv_data(file_obj, date_format, exclude_sps=[]):
    eppn_dict = {}

    for line in file_obj:
        eppn, d = line.decode("utf-8").strip().split(",")
        exp_date = datetime.strptime(d, date_format)
        eppn_dict[eppn] = exp_date
    # iterate over all local resources
    qs = Resource.objects.filter(realm_idp=settings.SERVER_REALM)
    if exclude_sps:
        qs = qs.exclude(sp__uuid__in=exclude_sps)
    for res in qs.select_related("sp", "sp__localresourcepolicy"):
        try:
            exp_date = eppn_dict[res.eppn]
        except KeyError:
            # eppn does no longer exist, we need to make sure that
            #   the resource will be locked immediately
            # deletion and purging happens as usual
            exp_date = min(res.expiry_date, timezone.now() - timedelta(days=1))
        # we might have a different time zone in the future, for eaxmpe now is
        # in standard time and deletion date is in summer time. We want to keep
        # the same local time, so we split time and date, add the days and
        # combine them
        delete_date = timezone.make_aware(
            datetime.combine(
                exp_date.date() + timedelta(days=res.sp.delete_after_days),
                exp_date.time()
            )
        )
        if timezone.is_naive(delete_date):
            delete_date = timezone.make_aware(delete_date)
        purge_date = delete_date + timedelta(days=res.sp.purge_api_data_after_days)
        if timezone.is_naive(exp_date):
            exp_date = timezone.make_aware(exp_date)

        if purge_date < timezone.now():
            uuid = res.uuid
            is_local_sp = res.sp.is_local
            res.delete()
            # if this resource belongs to a remote sp, we need to notify
            # the sp that we deleted the resource record
            if not is_local_sp:
                dispatch_deletion_sync(res, uuid)
        else:
            if res.expiry_date != exp_date or res.deletion_date != delete_date:
                res.update_attribute([
                    {"name": "expiry_date", "value": exp_date.isoformat()},
                    {"name": "deletion_date", "value": delete_date.isoformat()},
                ], user_type=UserType.TYPE_IDP)
