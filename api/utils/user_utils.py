# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re
try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin

from django.conf import settings
from django.urls import get_script_prefix
from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse

from api.models import TokenAuthUser, UserType, Resource


def extract_idp(eppn):
    return ".".join(re.split("\.|@", eppn)[-2:])


def get_local_url():
    return settings.HTTP_SERVER_NAME


def get_local_token():
    user = TokenAuthUser.objects.get(user_type=UserType.TYPE_API)
    token = Token.objects.get(user_id=user)
    return token.key


def get_remote_url_by_realm(realm):
    user = TokenAuthUser.objects.filter(user_type=UserType.TYPE_SYNC, realm=realm).first()
    if user:
        return user.api_url

    return None


def get_remote_url_by_res_uuid(uuid):
    res = Resource.objects.get(uuid=uuid)

    if res.sp.realm == settings.SERVER_REALM:
        # We are acting as SP here
        return get_remote_url_by_realm(res.realm_idp)
    else:
        # We are acting as IdP here
        return get_remote_url_by_realm(res.sp.realm)


def get_remote_token_by_realm(realm):
    user = TokenAuthUser.objects.filter(realm=realm, user_type=UserType.TYPE_SYNC).first()
    if user:
        token = Token.objects.get(user_id=user)
        return token.key

    return ""


def get_remote_token_by_res_uuid(uuid):
    res = Resource.objects.get(uuid=uuid)

    if res.sp.realm == settings.SERVER_REALM:
        # We are acting as SP here
        return get_remote_token_by_realm(res.realm_idp)
    else:
        # We are acting as IdP here
        return get_remote_token_by_realm(res.sp.realm)


def local_reverse(*args, **kwargs):
    r = reverse(*args, **kwargs)
    local_prefix = get_script_prefix()

    if r.startswith(local_prefix):
        r = r[len(local_prefix):]

    return urljoin(get_local_url(), r)
