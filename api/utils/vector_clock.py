# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class VectorClock(object):
    """
    This class implements a basic vector clock (VC) with an arbitrary number of components.
    See https://en.wikipedia.org/wiki/Vector_clock for details.
    """
    def __init__(self, *components):
        self.comp = components

    def happened_before(self, other):
        if isinstance(other, VectorClock) and len(self.comp) == len(self.comp):
            merged = zip(self.comp, other.comp)
            found_one = False

            for c in merged:
                if c[0] > c[1]:
                    return False
                if c[0] < c[1]:
                    found_one = True

            return found_one

        else:
            raise NotImplementedError  # pragma: no cover

    def concurrent_to(self, other):
        if isinstance(other, VectorClock) and len(self.comp) == len(self.comp):
            if not self.happened_before(other) and not other.happened_before(self):
                return True

            return False
        else:
            raise NotImplementedError  # pragma: no cover

    def merge(self, other):  # Side effect func
        if isinstance(other, VectorClock) and len(self.comp) == len(self.comp):
            merged = zip(self.comp, other.comp)
            self.comp = tuple(map(lambda t: max(t[0], t[1]), merged))

            # possible alternative implementation
            # self.comp = tuple([
            #     max(tupl)
            #     for tupl in zip(self.comp, other.comp)
            # ])

            return self.comp
        else:
            raise NotImplementedError  # pragma: no cover

    def __str__(self):
        return str(self.comp)


class VersionVector(VectorClock):
    """
    This class implements version vectors (VV) with an arbitrary number
    of components that are based on vector clocks.
    See https://en.wikipedia.org/wiki/Version_vector for details.
    Since the only difference relies in how VV's are updated,
    no implementation changes are necessary at this place.
    """

    @property
    def sp(self):
        return self.comp[0]

    @property
    def idp(self):
        return self.comp[1]
