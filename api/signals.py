# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.dispatch import Signal

resource_created = Signal()

resource_updated = Signal()

