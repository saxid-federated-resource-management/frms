# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import re_path
from rest_framework.urlpatterns import format_suffix_patterns

from api import views

urlpatterns = [
    re_path(r'^$', views.WelcomeView.as_view()),
    re_path(r'^health/$', views.Health.as_view(), name='health'),
    re_path(r'^csv_upload/$', views.CsvUploadView.as_view(), name='csv_upload'),
    re_path(r'^services/$', views.ServiceList.as_view(), name='service_list'),
    re_path(r'^services/(?P<uuid>[^/]+)/$',
        views.ServiceDetail.as_view(), name='service_detail'),
    re_path(r'^res/$', views.ResourceList.as_view(), name='resource_list'),
    re_path(r'^res/(?P<uuid>[^/]+)/$',
        views.ResourceDetail.as_view(), name='resource_detail'),
    re_path(r'^res/(?P<uuid>[^/]+)/attributes/$',
        views.ResourceAttributes.as_view(), name='resource_attributes')
]

urlpatterns = format_suffix_patterns(urlpatterns)
app_name = 'api'
