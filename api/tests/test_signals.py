# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from datetime import date, timedelta
from django.utils import timezone

from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from api.models import Attribute, Resource, UserType, TokenAuthUser, ServiceProvider
from api.signals import resource_created, resource_updated


class ResourceUpdatedTest(APITestCase):
    def setUp(self):
        TokenAuthUser.objects.create_superuser(username="a_super",
                                               email="test@a.edu",
                                               password="a_super",
                                               user_type=UserType.TYPE_API,
                                               api_url="http://api.a.edu:7000/",
                                               realm="a.edu")

        TokenAuthUser.objects.create_superuser(username="b_sync",
                                               email="test@b.edu",
                                               password="b_sync",
                                               user_type=UserType.TYPE_SYNC,
                                               api_url="http://api.b.edu:7001/",
                                               realm="b.edu")

        sp = ServiceProvider.objects.create(name="TestService",
                                            expiry_date=timezone.now(),
                                            realm="a.edu")

        self.res = Resource.objects.create(setup_token="Lorem ipsum",
                                           eppn="u1@b.edu",
                                           expiry_date=timezone.now(),
                                           deletion_date=timezone.now(),
                                           sp=sp)

        self.attrib = Attribute.objects.create(name="Quota",
                                               default_value="100",
                                               value=50,
                                               resource=self.res,
                                               vec_sp=1,
                                               vec_idp=0)

        self.resource_update_signals = []
        resource_updated.connect(self._resource_updated_called)

    def teardown(self):
        resource_updated.disconnect(self._resource_updated_called)

    def _resource_updated_called(self, sender, resource, user_type, **kwargs):
        self.resource_update_signals.append((sender, resource, user_type))

    def test_attribute_update(self):
        url = reverse('api:resource_attributes', args=[self.res.uuid])

        user = TokenAuthUser.objects.get(username='a_super')
        self.client.force_authenticate(user=user, token=user.token())

        data = [
            {
                "name": "Quota",
                "value": "50"
            },
        ]

        self.client.put(url, data=data, format='json')

        self.assertEqual(len(self.resource_update_signals), 0)

        data = [
            {
                "name": "Quota",
                "value": "60"
            },
            {
                "name": "cpuTime",
                "value": 42
            }
        ]

        self.client.put(url, data=data, format='json')

        self.assertEqual(len(self.resource_update_signals), 1)
        self.assertEqual(self.resource_update_signals[0][1], self.res)
        self.assertEqual(self.resource_update_signals[0][2], user.user_type)


class ResourceCreatedTest(APITestCase):

    def setUp(self):
        TokenAuthUser.objects.create_superuser(username="a_super",
                                               email="test@a.edu",
                                               password="a_super",
                                               user_type=UserType.TYPE_API,
                                               api_url="http://api.a.edu:7000/",
                                               realm="a.edu")

        self.sp = ServiceProvider.objects.create(name="TestService",
                                            expiry_date=timezone.now(),
                                            realm="a.edu")
        self.created_signal_calls = []
        resource_created.connect(self._created_fired)

    def teardown(self):
        resource_created.disconnect(self._created_fired)

    def _created_fired(self, sender, resource, user_type, **kwargs):
        self.created_signal_calls.append((sender, resource, user_type))

    def test_create_resource(self):
        url = reverse('api:resource_list')
        user = TokenAuthUser.objects.get(username='a_super')
        self.client.force_authenticate(user=user, token=user.token())

        today = date.today()
        data = {
            "eppn": "foo@a.edu",
            "sp_primary_key": "12345",
            "expiry_date": (today + timedelta(days=7)).strftime('%Y-%m-%dT%H:%M:%SZ'),
            "deletion_date": (today + timedelta(days=7+30)).strftime('%Y-%m-%dT%H:%M:%SZ'),
            "sp": self.sp.uuid,
            "setup_token": "1234",
        }

        self.client.post(url, data=data, format='json')
        res = Resource.objects.get(sp_primary_key="12345")
        self.assertEqual(len(self.created_signal_calls), 1)
        self.assertEqual(self.created_signal_calls[0][1], res)
