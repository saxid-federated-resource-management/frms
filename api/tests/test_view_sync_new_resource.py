# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.utils import timezone
import requests_mock
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from api.models import TokenAuthUser, UserType, Resource, ServiceProvider, Attribute


class SyncNewResourceTest(APITestCase):
    def setUp(self):
        TokenAuthUser.objects.create_superuser(username="a_super",
                                               email="test@a.edu",
                                               password="a_super",
                                               user_type=UserType.TYPE_API,
                                               api_url="http://api.a.edu:7000/",
                                               realm="a.edu")

        self.sync_user = TokenAuthUser.objects.create_superuser(username="b_sync",
                                               email="test@b.edu",
                                               password="b_sync",
                                               user_type=UserType.TYPE_SYNC,
                                               api_url="http://api.b.edu:7001/",
                                               realm="b.edu")

        sp = ServiceProvider.objects.create(name="TestService",
                                            expiry_date=timezone.now(),
                                            realm="a.edu")

        self.res = Resource.objects.create(setup_token="Lorem ipsum",
                                           eppn="u1@b.edu",
                                           expiry_date=timezone.now(),
                                           deletion_date=timezone.now(),
                                           sp=sp)

        self.attrib = Attribute.objects.create(name="Quota",
                                               default_value="100",
                                               value=50,
                                               resource=self.res,
                                               vec_sp=1,
                                               vec_idp=0)

    def test_sync_new_resource(self):
        url = reverse('api:resource_list')
        
        user = TokenAuthUser.objects.get(username='b_sync')
        self.client.force_authenticate(user=user, token=user.token())
        queue_data = """
        {"uuid":"fefe91d1-2e35-4496-9fcc-bec023a96616","attributes":[{"name":"quota","default_value":"","value":"0","vec_sp":1,"vec_idp":0},{"name":"quota_used","default_value":"","value":"0","vec_sp":1,"vec_idp":0},{"name":"quota_free","default_value":"","value":"0","vec_sp":1,"vec_idp":0}],"setup_token":"1234","eppn":"foobar@b.edu","realm_idp":"b.edu","added":"2017-11-29T12:10:30.231417Z","updated":"2017-11-29T12:10:30.231442Z","expiry_date":"2017-12-06T00:00:00Z","deletion_date":"2018-01-05T00:00:00Z","sp_primary_key":"luehr@hs-mittweida.de","sp":"eff887b0-319d-4975-ab5e-ea1ca4282eab"}
        """
        response = '{"uuid":"eff887b0-319d-4975-ab5e-ea1ca4282eab","name":"box.tu-chemnitz.de","realm":"tu-chemnitz.de","expiry_date":"2099-12-30T23:00:00Z","default_delete_after_days":200,"default_purge_api_data_after_days":90}'
        with requests_mock.Mocker() as m:
            m.get("http://api.b.edu:7001/services/eff887b0-319d-4975-ab5e-ea1ca4282eab/", text=response)
            r = self.client.post(url, queue_data, content_type="application/json")
        self.assertEqual(r.status_code, 201)
        res = Resource.objects.get(uuid="fefe91d1-2e35-4496-9fcc-bec023a96616")
        sp = ServiceProvider.objects.get(uuid="eff887b0-319d-4975-ab5e-ea1ca4282eab")
        self.assertEqual(res.sp, sp)
