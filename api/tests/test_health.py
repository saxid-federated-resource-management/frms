# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from dateutil.relativedelta import relativedelta
from django.core.management import call_command
from django.utils import timezone
from django.test import TestCase
import requests
import requests_mock
from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
import uuid

from api.models import QueueJob, QueueJobStatus, QueueJobType, TokenAuthUser, UserType


class AttributeUpdateTest(APITestCase):
    def setUp(self):
        TokenAuthUser.objects.create_superuser(username="a_super",
                                               email="test@a.edu",
                                               password="a_super",
                                               user_type=UserType.TYPE_API,
                                               api_url="http://api.a.edu:7000/",
                                               realm="a.edu")

        TokenAuthUser.objects.create_superuser(username="b_sync",
                                               email="test@b.edu",
                                               password="b_sync",
                                               user_type=UserType.TYPE_SYNC,
                                               api_url="http://api.b.edu:7001/",
                                               realm="b.edu")

    def test_health(self):
        url = reverse('api:health')

        user = TokenAuthUser.objects.get(username='a_super')
        token = Token.objects.get(user_id=user)
        self.client.force_authenticate(user=user, token=token)
        response = self.client.get(url, format='json')
        self.assertEqual(response.data, user.username)

        user = TokenAuthUser.objects.get(username='b_sync')
        token = Token.objects.get(user_id=user)
        self.client.force_authenticate(user=user, token=token)
        response = self.client.get(url, format='json')
        self.assertEqual(response.data, user.username)


class TestHealthCommand(TestCase):
    def test_no_errors_exit_with_0(self):
        with self.assertRaises(SystemExit) as ex:
            call_command("check_health")
        self.assertEqual(ex.exception.code, 0)

    def test_11_min_old_queuejob_exist_with_1(self):
        old = timezone.now() - relativedelta(minutes=11)
        print(old)
        qj = QueueJob(
            added=old,
            res_uuid=uuid.uuid4(),
            obj="{}",
            status=QueueJobStatus.STATUS_READY,
            type=QueueJobType.TYPE_RES,
            realm="foo.bar"
        )
        qj.full_clean()
        qj.save()
        # added is auto_add_now field. lets fake it.
        qj.added = old
        qj.save()
        print(qj.added)
        with self.assertRaises(SystemExit) as ex:
            call_command("check_health")
        self.assertEqual(ex.exception.code, 1)

    def test_remote_error_fails_with_1(self):
        remote_url = 'https://remote.example.org/api'
        remote_realm = 'foo.bar'
        remote_user = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm=remote_realm, api_url=remote_url)
        remote_user.set_unusable_password()
        remote_user.full_clean()
        remote_user.save()

        with requests_mock.Mocker() as m:
            m.register_uri("GET", remote_user.remote_reverse("api:health"), status_code=500)
            with self.assertRaises(SystemExit) as ex:
                call_command("check_health")
        self.assertEqual(ex.exception.code, 1)

    def test_remote_not_available_fails_with_1(self):
        remote_url = 'https://remote.example.org/api'
        remote_realm = 'foo.bar'
        remote_user = TokenAuthUser(user_type=UserType.TYPE_SYNC, username='foobar', realm=remote_realm, api_url=remote_url)
        remote_user.set_unusable_password()
        remote_user.full_clean()
        remote_user.save()

        with requests_mock.Mocker() as m:
            m.register_uri("GET", remote_user.remote_reverse("api:health"), exc=requests.exceptions.ConnectTimeout)
            with self.assertRaises(SystemExit) as ex:
                call_command("check_health")
        self.assertEqual(ex.exception.code, 1)
