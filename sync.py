#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from copy import copy
import multiprocessing
import os
import sys
import signal
import logging
from logging.handlers import TimedRotatingFileHandler
import requests
import argparse

import django
from django.db import transaction, connection, connections
from django.db.utils import DatabaseError

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "frms.settings")
django.setup()


from api.models import TokenAuthUser, UserType, QueueJob, QueueJobStatus, QueueJobType
from api.utils.queue_utils import sync_resource, sync_attributes, delete_resource, SyncException

logger = logging.getLogger(__name__)
# Disable logging originating from requests lib
logging.getLogger("requests").setLevel(logging.WARNING)


class SyncProcess(multiprocessing.Process):
    _POLL_TIME = 5

    def __init__(self, realm, stop_event, ex_q):
        # close connection to prevent odd django connection handling,
        # it will be automatically reopened
        logger.debug("SyncProcess init started")
        connections.close_all()
        signal.signal(signal.SIGTERM, self.shutdown)
        signal.signal(signal.SIGINT, self.shutdown)

        multiprocessing.Process.__init__(self)
        self._realm = realm
        self._stop_event = stop_event
        self._ex_q = ex_q

        logger.debug("SyncProcess init ready")

    def create_individual_logger(self):
        """
        create a seperate logger file per realm

        it closes all TimedRotatingFileHandler instances and reopens them with
        a different file name
        """
        self._logger = copy(logging.getLogger())

        handlers = [hdl
                    for hdl in self._logger.handlers
                    if isinstance(hdl, TimedRotatingFileHandler)]
        logger.info("found {} handlers".format(len(handlers)))
        for hdl in handlers:
            self._logger.removeHandler(hdl)
            basename = hdl.baseFilename.split(".log")[0]
            name = "{}_{}.log".format(basename, self._realm)
            process_hdl = TimedRotatingFileHandler(name)
            self._logger.addHandler(process_hdl)

    def run(self):
        self.create_individual_logger()
        try:
            self._logger.info("Sync process started for realm: {realm}, PID: {pid}."
                              .format(realm=self._realm, pid=self.pid))
            cursor = connection.cursor()

            realm_user = TokenAuthUser.objects.get(realm=self._realm,
                                                   user_type=UserType.TYPE_SYNC)

            while not self._stop_event.is_set():
                try:
                    objects_filter = QueueJob.objects.filter(realm=self._realm,
                                                            status=QueueJobStatus.STATUS_READY)
                    #reopen connection if closed by server shutdown
                    if not connection.is_usable():
                        connection.close()
                        cursor = connection.cursor()

                    for job_id in objects_filter.values_list("id", flat=True):
                        if self._stop_event.is_set():
                            self._logger.info("Sync process stopped.")
                            cursor.close()
                            return

                        try:
                            with transaction.atomic():
                                self._logger.info("Processing job id: {}.".format(job_id))
                                job = QueueJob.objects.get(id=job_id)

                                if job.type == QueueJobType.TYPE_RES:
                                    sync_resource(realm_user, job.obj)
                                elif job.type == QueueJobType.TYPE_ATT:
                                    sync_attributes(realm_user, job.obj, job.res_uuid)
                                elif job.type == QueueJobType.TYPE_DEL:
                                    delete_resource(realm_user, job.res_uuid)
                                else:
                                    raise NotImplementedError("Job type is not supported.")

                                job.status = QueueJobStatus.STATUS_FINISHED
                                job.save()
                        except requests.exceptions.RequestException:
                            pass
                            self._logger.warning("Host not reachable, skipping...")
                        except SyncException as e:
                            self._logger.warning(e)
                        except DatabaseError as e:
                            # database connection might have died
                            self._logger.warning(e)
                except DatabaseError as e:
                    # database connection might have died
                    self._logger.warning(e)

                self._stop_event.wait(self._POLL_TIME)
                self._logger.debug("Loop finished.")

            self._logger.info("Sync process stopped.")
        except KeyboardInterrupt:
            pass
        except Exception as e:
            self._logger.exception("Exception in sync process.")
            self._ex_q.put(e)

    def shutdown(self):
        logger.info("Called stop on sync process for realm: {}.".format(self._realm))
        self._stop_event.set()


class Main(object):
    def _null_signal_handler(self, *args, **kwargs):
        signal.signal(signal.SIGTERM, self._null_signal_handler)
        signal.signal(signal.SIGINT, self._null_signal_handler)

    def quit_gracefully(self):
        self._stop_event.set()
        for p in self._processes:
            p.shutdown()
            logger.info("Terminating PID: {}".format(p.pid))
        for p in self._processes:
            p.join()

        # Remove PID file
        os.unlink(self.PIDFILE)

        logger.info("Exit.")
        sys.exit(0)

    def quit_gracefully_handler(self, signal, frame):
        self._null_signal_handler()
        logger.warning('Syncer received signal "{}", preparing to quit...'.format(signal))
        self.quit_gracefully()

    def __init__(self):
        logger.info("Initializing...")

        # Determine PID file location based on OS
        if os.name == "nt":
            piddefault = os.path.join(os.getenv("APPDATA"), "syncer.pid")
        else:
            piddefault = "/var/run/syncer.pid"

        parser = argparse.ArgumentParser()
        parser.add_argument("--pidfile", default=piddefault)

        args = parser.parse_args()
        self.PIDFILE = args.pidfile

        if os.path.isfile(self.PIDFILE):
            logger.error("Found PID file ({}). Syncer is already running, aborting..."
                         .format(self.PIDFILE))
            sys.exit(1)

        with open(self.PIDFILE, "w") as f:
            f.write(str(os.getpid()))

        self._stop_event = multiprocessing.Event()
        self._ex_q = multiprocessing.Manager().Queue()
        self._processes = []

    def run(self):
        logger.info("Syncer started.")

        # Fetch all remote institutions
        remote_realms = list(TokenAuthUser.objects.filter(
            user_type=UserType.TYPE_SYNC).values_list("realm", flat=True))
        # close connection to prevent odd django connection handling,
        # it will be automatically reopened
        connections.close_all()

        for realm in remote_realms:
            logger.debug("Start SyncProcess for realm {}".format(realm))
            p = SyncProcess(realm, self._stop_event, self._ex_q)
            p.daemon = True
            p.start()
            self._processes.append(p)

        signal.signal(signal.SIGINT, self.quit_gracefully_handler)
        signal.signal(signal.SIGTERM, self.quit_gracefully_handler)

        try:
            ex_res = self._ex_q.get()
            if ex_res:
                logger.error("At least one sync process died, preparing to quit...")
                self.quit_gracefully()
        except KeyboardInterrupt:
            pass

if __name__ == "__main__":
    Main().run()
