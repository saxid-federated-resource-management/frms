from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin


urlpatterns = [
    re_path(r'^', include('api.urls', namespace='api')),
    re_path(r'^user/', include('usergui.urls', namespace="usergui")),
    re_path(r'^admin/', admin.site.urls),
]
