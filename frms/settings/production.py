# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import sys
import os.path
import os
import random
import string
from .common import *


DEBUG = False

ALLOWED_HOSTS = [
    'www-apps.hrz.tu-chemnitz.de',
    'www-apps.verwaltung.tu-chemnitz.de',
]

"""
Anpassungen für Shibboleth/REMOTE_USER

https://docs.djangoproject.com/en/1.8/howto/auth-remote-user/
"""
#MIDDLEWARE_CLASSES.append('django.contrib.auth.middleware.AuthenticationMiddleware')
MIDDLEWARE.append('django.contrib.auth.middleware.RemoteUserMiddleware')
MIDDLEWARE.append('usergui.middleware.RemoteUserEppnMiddleware')

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.RemoteUserBackend',
]


"""
Standard-Logging Konfiguration

https://docs.djangoproject.com/en/dev/topics/logging/
"""
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s %(levelname)s %(process)d %(processName)s "%(message)s"',
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
        'file_handler': {
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'backupCount': 14, # Logs 14 Tage lang aufheben
            'formatter': 'default',
            'filename': '/var/www/django/frms/logs/frms.log',
            'when': 'midnight',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        '': {
            'handlers': ['file_handler'],
            'level': 'INFO',
        }
    }
}


"""
Mindestens folgende Einstellungen werden im Produktivsystem unter
/var/www/django/frms/private/settings_private.py
gespeichert:

    - `DATABASE_PASSWORD`
    - `SECRET_KEY`
    - Sonstige Geheimnisse, wie LDAP-Passwörter o.ä.
"""
private_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "..", "..", "..", "private"))
sys.path.append(private_dir)

if os.environ.get("DOCKER", "") == "1":
    secrets_file = os.path.join(private_dir, "django_secret.txt")
    if not os.path.exists(secrets_file):
        key = "".join([random.SystemRandom().choice(string.digits + string.ascii_letters + "!#$%&()*+,-./:;<=>?@[]^_`{|}~") for i in range(60)])
        with open(secrets_file, "w") as f:
            f.write(key)
    with open(secrets_file, "r") as f:
        SECRET_KEY = f.readline().strip()
    AUTHENTICATION_BACKENDS = [
        'django.contrib.auth.backends.ModelBackend',
    ]
    MIDDLEWARE.append('django.contrib.auth.middleware.AuthenticationMiddleware')
    MIDDLEWARE.remove('django.contrib.auth.middleware.RemoteUserMiddleware')

    from settings_docker import *
else:
    from settings_private import *

# Mindestens eine Datenbank muss als 'default' konfiguriert werden.
    DATABASES = {
        'example_postgres': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'datenbank',
            'USER': 'datenbank_rw',
            'PASSWORD': DATABASE_PASSWORD, # @UndefinedVariable
            'HOST': 'pgsql.hrz.tu-chemnitz.de',
        },
        'example_mysql': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'datenbank',
            'USER': 'datenbank_rw',
            'PASSWORD': DATABASE_PASSWORD, # @UndefinedVariable
            'HOST': 'mysql.hrz.tu-chemnitz.de',
            'OPTIONS': {
                'init_command': ('SET storage_engine=INNODB,'
                                'character_set_connection=utf8,'
                                'collation_connection=utf8_bin'),
            }
        },
    }
    try:
        from settings_local import *
    except:
        pass
