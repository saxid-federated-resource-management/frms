
[![pipeline status](https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms/badges/master/pipeline.svg)](https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms/commits/master)
[![coverage report](https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms/badges/master/coverage.svg)](https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms/commits/master)


# FRM-Dienst

## Installation
* [Installationsanleitung](doc/deployment.md)
* [Neuen Service Provider einrichten](doc/new-service-provider.md)

## Annahmen

  * gegenseitiges Vertrauen unter Einrichtungen
  * Vorhandensein von Shibboleth im Rahmen der Harmonisierung der Identitätsmanagementsysteme zur gemeinsamen Nutzung von Benutzerkonten
  * Vorhandensein von Diensten, denen Shibboleth vorangestellt werden können, und die die Möglichkeit bieten, erweiterbar zu sein

## Anforderungen/Ziele

  * ein Dienst, der über eine REST-Schnittstelle verfügt, über die Ressourcen und deren Attribute registriert, aktualisiert, abgefragt und gelöscht werden können, sowie autonom als auch manuell dafür sorgt, dass unter den Einrichtungen ein synchronisierter Datenstand besteht
  * gegenseitiges Wissen über die tatsächliche Verwendung von teilbaren Ressourcen unter dem Aspekt der Datensparsamkeit
  * Erleichterung von Deployment und Withdrawal von Ressourcen
  * Skalierbarkeit → geringer notwendiger Nachrichtenrichtenaustausch für die Sicherstellung der Funktionalität (Synchronisation)
  * Beziehungen: Nutzer ↔ Ressource:
    * Nutzer * ↔ 1 Ressourceninstanz (Beispiele: ownCloud, E-Mail)
    * Nutzer 1 ↔ * Ressourceninstanz (Beispiele: HPC)
  * Authentizität der initialen Nutzanfrage eines Dienstes muss jeweils sichergestellt werden, indem Shibboleth einen `setupToken` mitsendet, der mithilfe dessen PKI signiert wurde und wesentliche Informationen über den Antragsteller beinhaltet.

## Konzept

[Beschreibung des Datenflusses](doc/dataflow.md)

Mehrere Service Provider (SP) stellen Dienste (Ressourcen) bereit.

Über das Shibboleth-Äquivalent eines Nutzernamens können Nutzer einer Einrichtung über den einzigartigen ePPN (eduPersonPrincipalName) eindeutig identifizert werden.

Die Einrichtungen A und B verfügen jeweils über einen FRM-Dienst FRMS_A und FRMS_B.

Möchte Nutzer U_A1 aus Einrichtung A den Fremddienst S_B1 von Einrichtung B nutzen, erfolgt zunächst über Shibboleth die einrichtungsübergreifende Authentifizierung, bei der Shibboleth an den Dienst den `setupToken` mitschickt.

Der Dienst S_B1 erstellt/initialisiert daraufhin intern für sich die Ressource, verifiziert die Signatur des abgefangenen `setupToken` und erstattet bei Erfolg mithilfe ein im Dienst integriertes Plugin/Hook dem FRM-Dienst FRMS_B darüber Bericht, wobei der `setupToken` archivierend mitgesendet wird.

FRMS_B komplettiert diesen Eintrag in seiner Datenbank und legt eine Synchronisationsaufgabe in seine Warteschlange, wodurch der Eintrag zu einem späteren Zeitpunkt, sobald möglich, mit dem FRMS_A der Einrichtung A synchronisiert wird.

(Werden in einem kurzen Abstand mehrere Sync-Aufträge in die Warteschlange eines FRM-Dienstes zur Synchronisation gelegt, werden jene zu einem einzigen zusammengefasst, die dieselbe Zieleinrichtung besitzen und zu einem späteren Zeitpunkt gesendet.)

Der empfangende FRM-Dienst bestätigt direkt empfangene Sync-Aufträge, damit diese auf der Gegenseite aus der Warteschlange entfernt werden können.

Ändert Nutzer A an seiner Einrichtung etwa die Attribute, die den Fremddienst S_B1 der Einrichtung B betreffen, so werden diese Änderungen zunächst lokal wieder an den FRMS_A mitgeteilt, der den Synchronisationsauftrag wieder wie oben beschrieben in seine Warteschlange anstellt.

Erlischt die ePPN bei einem Fremd-IdP, ändern sich Fremdressourcenattribute, oder werden Ressourcen eines Dienstes gelöscht, so sind diese Änderungen an alle lokalen Dienste weiterzuleiten, bzw. je nach vorig beschrieben Fall direkt über die Plugins der Dienste als nächsten Schritt direkt an den FRM-Dienst der lokalen Einrichtung weiterzuleiten, der dann den folgenden Synchronisationsprozess übernimmt.

Anfragen an den FRM-Dienst dürfen nicht abgelehnt werden, außer bei konkurrierenden Änderungen. Der Umgang mit Anfragen, die das Vorhandensein von physischen oder logischen Ressourcen übersteigen, obliegt demnach nicht dem FRM-Dienst.

Bei nebenläufigen Änderungen die durch die Verwendung von einer Vektoruhr für jedes Attribut festgestellt werden können, werden Änderungen ausgehend vom Service Provider (hier B) präferiert.
Änderungen am Soft- und Hardlimit einer Ressource obliegen exklusiv der IdP-Seite (hier A).

Bei jeder lokalen Änderung wird die Vektorkomponente, die der eigenen Rolle bezüglich einer Ressource entspricht, um 1 erhöht.

Beim Empfang einer Nachricht (Synchronisation), erhöht die Empfängerseite wiederum ihre Vektorkomponente um 1 und setzt anschließend *max*(VeC-SP_i, VeC-IdP_i) für alle i-ten Komponenten.

Sollten nun Änderungen auf beiden Seiten geschehen seien, so ist beim Vergleich der Vektoruhren festzustellen, dass keiner der beiden Vektoren echt größer ist als der jeweils andere. Je nach Ursprung des Synchronisationsauftrages wird nach dem Prüfen die Anfrage demnach direkt abgelehnt bzw. aufgrund der oben beschriebenen Präferenz übernommen.

## Weitere Use-Cases

  * Welche (Standard)Attribute hat eine bestimmte Ressource?

## Beispielkommunikation (Initialisierung einer Ressource)

Szenario siehe Konzept.

1. Plugin überprüft Signatur des `SetupToken`, erstellt intern die Ressource und über das Plugin wird die neu angelegte Ressource inklusive den im Plugin konfigurierten Standardattributen dem lokalen FRM-Dienst mitgeteilt.

    ```http
    POST /res HTTP/1.1
    Host: api.b.edu
    Authorization: Token Token_S_B1

    {
        "setup_token": "-----BEGIN PGP SIGNED MESSAGE-----\n...",
        "eppn": "u1@a.edu",
        "realm": "a.edu"
        "sp": "1275c961-e74d-478f-8afe-da53cf4189e3",
        "deletion_date": "31-12-2020 12:00",
        "expiry_date": "1-12-2020 12:00",
        "attributes": [{
            "name": "DiskQuota",
            "default_value": 100,
            "value": 500
        }]
    }
    ```

2. Der FRM-Dienst erstellt anhand der Anfrage intern den Eintrag und reiht einen Sync-Auftrag in seine Queue ein.

3. Der Sync-Auftrag wird an Einrichtung A (Zuordnung über `realm` und `api_url` in `TokenAuth`) gesendet:

    ```http
    POST /res HTTP/1.1
    Host: api.a.edu
    Authorization: Token Token_FRMS_B

    {
        "setup_token": "-----BEGIN PGP SIGNED MESSAGE-----\n...",
        "eppn": "u1@a.edu",
        "realm": "a.edu"
        "sp": "1275c961-e74d-478f-8afe-da53cf4189e3",
        "deletion_date": "31-12-2020 12:00",
        "expiry_date": "1-12-2020 12:00",
        "attributes": [{
            "name": "DiskQuota",
            "default_value": 100,
            "value": 500
        }]
    }
    ```

4. Einrichtung A prüft, noch ohne eine HTTP-Response gegeben zu haben, ob der in der Anfrage referenzierte SP in der lokalen Datenbank bereits vorhanden ist. Ist dies nicht der Fall, wird eine Anfrage an Einrichtung B gesendet, um den fehlenden SP-Eintrag zu holen:

    ```http
    GET /services/1275c961-e74d-478f-8afe-da53cf4189e3 HTTP/1.1
    Host: api.b.edu
    Authorization: Token Token_FRMS_A
    ```

    Antwort von B:

    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json


    {
      "name": "B's ownCloud #1",
      "UUID": "1275c961-e74d-478f-8afe-da53cf4189e3",
      "expiry_date": ...,
    }
    ```

5. Einrichtung A bestätigt Anfrage, Einrichtung B entfernt Sync-Auftrag aus Queue:

    ```http
    HTTP/1.1 201 CREATED
    Location: /res/9945c961-e74d-478f-8afe-da53cf4189e3
    ```
    **oder**: Einrichtung A antwortet nicht, sodass der Sync-Auftrag in der Queue um eine Position verschoben wird, um den Vorgang später zu wiederholen, siehe oben.


## Beispielkommunikation (Aktualisierung der Attribute einer Ressource)

Einrichtung A (IdP-Seite) möchte nun Änderungen an den Attributen einer Ressource vornehmen, die von Einrichtung B angeboten wird.

```http
PUT /res/9945c961-e74d-478f-8afe-da53cf4189e3/attributes HTTP/1.1
Host: api.b.edu
Authorization: Token Token_A

[{
    "name": "DiskQuota",
    "value": 500,
    "vec_sp": 1,
    "vec_idp": 2
}]
```

Der Eintrag innerhalb Sync-Queue von A wird entfernt, sobald B mit `201` geantwortet hat (wenn die Änderung nicht nebenläufig war).
Bei einem `304`-Fehler wird der Queue-Eintrag zurückgestellt bzw. hinten angereiht.

## Beschreibung des Datenmodells

Jeder FRM-Dienst verfügt über das gleiche Datenmodell.

### `TokenAuth`

  * Token (einmalig generierter Token als Shared Secret zur Authentifizierung und Zuordnung genutzt wird)
  * Realm
  * UserType (API (Anfragen an sich selbst bzw. Django Superuser), SP (Token-Auth separat für SPs) oder SYNC (Tokens fremder IdPs/Einrichtungen))
  * API_URL (URL der REST-Ressource/des FRM-Dienstes)
  * SP (Fremdschlüssel)

### `SP`

  * UUID (UUID4, eindeutiger Identifier)
  * Name (Bezeichnung des Service Providers)
  * Ablaufdatum
  * Lokal (ob lokaler SP oder nicht)

### `Resource`

Ressourceneinträge bestehen aus folgenden (DB-)Attributen:

  * UUID (UUID4, eindeutiger Identifier)
  * eppn des Antragstellers
  * Realm der Einrichtung des SP-Standortes
  * SetupToken (zur Verfikation der Echtheit des ursprünglichen Antrages)
  * Added (Erstelldatum)
  * Updated (Änderungsdatum, keine Referenz für Synchronisation)
  * Ablaufdatum (Softlimit)
  * Löschdatum (Hardlimit)
  * SP (Fremdschlüssel)

Zu jedem Ressourceneintrag gehören zudem:

### `Attribute`

Beispiele für derartige Attribute: Quota, MaxCPUTime etc.

  * DefaultValue (Standardwert des Attributs)
  * Value (aktueller Wert des Attributs)
  * Name (Name des Attributs)
  * Resource (Fremdschlüssel)
  * VeC-SP (wird bei jeder Änderung iteriert, erlaubt Nebenläufigkeitsfeststellung)
  * VeC-IdP
  * Priorität (welche Rolle wird im Fall eines Konflikts priorisiert? Standardmäßig SP)

## REST-API Details / URL-Schema TODO

Der eigene (lokal zugeordnete) Token zur Authentifizierung und Zuordnung der Einrichtungen untereinander muss bei allen HTTP-Anfragen mitgeschickt werden:

`Authorization: Token 5945c961-e74d-478f-8afe-da53cf4189e3`

|REST-Ressource|Semantik (aus lokaler Sicht)|
|---|---|
|/services|`GET`: Liste aller Service Provider (Dienste), die anderen Einrichtungen zur Verfügung stehen |
| | (alle Einträge aus `SP` für die gilt: `Lokal` = True) |
|/services/:uuid | Details zu einem Service Provider, der anderen Einrichtungen zur Verfügung steht
|/res|`POST`: Erstellt lokal neue Ressource anhand eines `SetupToken` und Metadaten, die durch das Plugin eines Dienstes mitgesendet werden |
| | (Unterscheidung ob initiale Erstellung oder Synchronisation erfolgt über den Token im `Authorization`-Header) |
| | Felder: `SetupToken`, SP-UUID, Lösch- und Ablaufdatum, Array[Attributname:Wert] |
|/res|`POST`: Erstellt (synchronisiert) lokal neue Ressource anhand eines `SetupToken` und Metadaten, die durch einen einen fremden FRM-Dienst erzeugt wurden
| | Felder: `SetupToken`, SP-UUID, Lösch- und Ablaufdatum, Array[Attributname:Wert] |
|/res|`GET`: Liste aller Ressourcen und deren Attribute, die Nutzer der anfragenden Einrichtung lokal besitzen |
/res/queue|`GET`: wie /res/, aber nur Einträge, die sich noch in der Queue befinden |
|/res/:uuid/attributes |`GET`: Liste aller Attribute (inkl. Lösch- und Ablaufdatum) einer konkreten Ressource via `:uuid` |
|/res/:uuid/attributes |`PUT`: Aktualisiert Attribute (inkl. Lösch- und Ablaufdatum) einer konkreten Ressource via `:uuid` |

## Python Style Ref

```
module_name, package_name, ClassName, method_name, ExceptionName, function_name, GLOBAL_CONSTANT_NAME, global_var_name, instance_var_name, function_parameter_name, local_var_name
```

## Testabdeckung

[![coverage report](https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms/badges/master/coverage.svg)](https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms/commits/master)
