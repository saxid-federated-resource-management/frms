# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.core.management.base import BaseCommand

from webtrigger import post_resource_event
from api.models import Resource, ServiceProvider


class Command(BaseCommand):
    help = "manually fire create or update webtrigger for resources"

    def add_arguments(self, parser):
        parser.add_argument(
            '--event_type',
            help="either create or updated",
            choices=['create', 'update'],
            required=True,
        )
        parser.add_argument(
            '--only-sp',
            help="only fire event for resources of given service provider UUID",
        )
        parser.add_argument(
            'resources',
            nargs='*',
        )

    def handle(self, **options):
        event_type = options['event_type']
        if options['resources']:
            qs = Resource.objects.filter(pk__in=options['resources'])
        else:
            qs = Resource.objects.all()

        if options['only_sp']:
            sp = ServiceProvider.objects.get(pk=options['only_sp'])
            qs = qs.filter(sp=sp)

        for res in qs.iterator():
            post_resource_event(event_type, res)
