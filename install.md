# FRM-Dienst

## Installation

1. Abhängigkeiten installieren

    ```
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    ```

2. In `frms/settings.py` sowohl `HTTP_SERVER_NAME` als auch `SERVER_REALM` ähnlich der Vorgaben anpassen.

3. Superuser erstellen und als Typ `TYPE_API` einrichten

    ```
    python manage.py migrate
    python manage.py createsuperuser
    python manage.py runserver
    ```

    Unter http://localhost:8000/admin/api/tokenauthuser/ lässt sich nach Auswahl des eben erstellten Nutzers der Nutzertyp anpassen.

4. Lokal vorhandene Service Provider einrichten via
4.1 Admin-Interface: http://localhost:8000/admin/api/sp/add/
4.2 oder Konsole: `python manage.py create_sp`

5. Für die Synchronisation innerhalb mehrerer Einrichtungen müssen `TYPE_SYNC`-Nutzer angelegt werden, die einen gemeinsamen Token als _Shared Secret_ teilen.
    
    Der Token kann per Konsole via `python manage.py change_token` modifiziert werden.