Neuen Service-Provider hinzufügen
=================================

Voraussetzung: FRMS ist [deployt](deployment.md).

In der Django-Verwaltung bei Service Providers auf „hinzufügen“ klicken. Die
dortige UUID ergibt den Wert des Schlüssels ´sp´ im JSON-Dokument. Der Name
und das Ablaufdatum sind im Prinzip beliebig – ggf. kann letzteres auf einen
Abkündigungstermin eines Dienstes gesetzt werden. Das Realm sollte der Domain
 des Instituts entsprechen (z. B. tu-chemnitz.de bei der TU Chemnitz).

Anschließend bei Benutzer auf „hinzufügen“ klicken. Neben den erforderlichen
Angaben ist bei „User type“ *SP user* und bei „SP“ der zuvor erstellte
Service Provider auszuwählen. Im Admin-Bereich unter *Token* findet sich
der Token des Benutzers, welcher im `Authorization`-Header-Feld angegeben
wird.

Import-Skript
-------------

Ein Skript, welches Nutzer eines Dienstes in das FRMS importieren soll,
muss folgendes bewerkstelligen:

### Auslesen der Benutzer im FRMS

```
GET /res/
Authorization: Token f852ca6182fb4bccd36630a842aa745314648451
```
gibt ein JSON-Dokument mit den Benutzern zurück.

### Auslesen der Benutzer des Dienstes

(abhängig vom Dienst)

### Übertragen neuer Benutzer

Für alle Benutzer, die der Dienst enthält und die noch nicht im FRMS sind,
muss ein JSON-Dokument nach dem Schema dieses Beispiels übertragen werden:
```
POST /res/
Content-Type: application/json
Authorization: Token f852ca6182fb4bccd36630a842aa745314648451
```
```json
{
  "sp": "2b4fa031586045d6af07db5fd21933ed",
  "sp_primary_key": "b@tu-chemnitz.de",
  "eppn": "b@tu-chemnitz.de",
  "expiry_date": "2016-12-09T00:00:00Z",
  "deletion_date": "2017-01-08T00:00:00Z",
  "attributes": [
    {"name": "quota", "value": 3722502144},
    {"name": "quota_used", "value": 0},
    {"name": "quota_free", "value": 3722502144}
  ],
  "setup_token": "1234"
}
```

Sync-Skript
-----------

Um die Daten des Dienstes im FRMS zu aktualisieren oder Sperr- respektive
Löschfristen aus dem FRMS im Dienst durchzusetzen, muss das Skript
folgendes tun:

### Auslesen der Benutzer im FRMS

Siehe oben, hier ist noch zu beachten, dass man von jedem Benutzer auch noch
die UUID zwischenspeichern sollte, falls Attribute aktualisiert werden sollen.

### Auslesen der Benutzer im Dienst

(abhängig vom Dienst)

### Abgleich FRMS → Dienst

Für jeden Nutzer im FRMS muss die Sperr- sowie Löschfrist überprüft werden und
dementsprechend bei Überschreitung im Dienst die Sperrung bzw. Löschung
des Accounts angestoßen werden. (Letzteres ist wieder vom Dienst abhängig.)

### Abgleich Dienst → FRMS

Mit einem PUT-Request auf `/res/<UUID>/attributes/` mit einer JSON-Liste
an Attributen lassen die Attribute desjenigen Benutzers aktualisieren,
dessen Ressource die angegebene UUID hat. Beispiel anhand einer Quota:

```
PUT /res/e06d85a5-9396-4ccb-9d15-be4eb8882ebe/attributes/
Content-Type: application/json
Authorization: Token f852ca6182fb4bccd36630a842aa745314648451
```
```json
[
  {"name": "quota", "value": 1073741824},
  {"name": "quota_used", "value": 6543621},
  {"name": "quota_free", "value": 1067198203}
]
```

Jedes Attribut soll die Schlüssel `name` und `value`, ggf. auch
noch `default_value` enthalten.

### Behandlung der Benutzer, die nicht im FRMS sind

Schließlich sollten noch alle Benutzer, die im Dienst, aber nicht im FRMS sind,
blockiert werden.
