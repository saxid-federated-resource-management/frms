Integration in das eigene Servicemanagement
===========================================

Ziel ist es, dass die Nutzer das Servicemanagementportal ihrer Heimathochschule
nutzen können, um Dienste einer Fremdeinrichtung zu konfigurieren. Dies soll
ein nahtloses Benutzererlebnis sicherstellen. FRMS bietet dazu Django Signale,
die mit eigenen Erweiterungen von FRMS genutzt werden können, und eine
Webtrigger Implementierung, die eine konfigurierbare URL mit einen POST Request
beschickt, wenn Ressourcen angelegt oder geändert werden. 

Webtrigger
----------

Der Webtrigger funktioniert, sobald in den `settings` die Variable
`SAXID_RESOURCE_TRIGGER_URL` gesetzt wird. An diesen URL werden dann bei
Änderungen und Erstellen einer Ressource JSON Daten in folgendem Format
geschickt:

```json
    {
        "version": 1, 
        "event": "update", 
        "details": {
            "attributes": {
                "Quota": "60",
                "expiry_date": "2017-06-30T11:14:30Z",
                "deletion_date": 
            }, 
            "service_provider": "d26b2c23-3597-438b-a607-c75f67bf097b", 
            "eppn": "u1@b.edu", 
            "uuid": "d0126031-a6f4-4eec-943a-6c900ef95a84"
        }
    }
```

In den `settings` kann die Variable `SAXID_RESOURCE_TRIGGER_HEADERS` (dict)
gesetzt werden. Die enthaltenen Header werden dann mit an den Server geschickt.
Dies kann zum Beispiel für Authorization Header genutzt werden.

Eigene Erweiterungen mit Django Bordmitteln
-------------------------------------------

Wenn der Webtrigger nicht reicht, kann man eigene Signal Receiver
implementieren. Die Signale sind in [api/signals.py](../api/signals.py)
definiert. Der Code für den Webtrigger in
[webtrigger/__init__.py](../webtrigger/__init__.py) kann als Beispiel gesehen
werden.
