Deployment eines API Servers
============================

Systemvoraussetzungen:
* 1 VM mit Linux, per HTTPS von beteiligten Hochschulen erreichbar
* 1 CPU
* 2 GB RAM
* 20 GB Platte
Idealerweise bekommt die Maschine einen CNAME, damit bei Plattformupgrades der Name stabil bleiben kann


Debian/Ubuntu vorbereiten
-------------------------

```bash
apt-get install uwsgi uwsgi-plugin-python3 libapache2-mod-uwsgi git python3-venv build-essential python3-dev
adduser --system saxid
a2enmod allowmethods headers proxy proxy_uwsgi ssl
a2ensite default-ssl
systemctl restart apache2
```

PostgreSQL einrichten
---------------------

Wenn die Datenbank lokal auf dem SaxID Server laufen soll, ist PostgreSQL die Empfehlung.

```bash
apt-get install postgresql libpq-dev
su - postgres
# Datenbanknutzer anlegen
createuser saxid_rw -P
# Datenbank anlegen
createdb -O saxid_rw saxid
exit
```


SaxID API deployen
------------------

Der Code vom Projekt ist unter
https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms


```bash
for dir in env private static logs uwsgi; do
  mkdir -p /var/www/django/frms/$dir
done
chown saxid logs uwsgi
```

Datei /var/www/django/frms/private/settings_private.py mit folgendem Inhalt:

```python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

SECRET_KEY = "XYZ..." #60 Stellen Zufall für Django

DATABASE_PASSWORD = "XXXXX"
```
`SECRET_KEY` kann folgendermaßen generiert werden:
```python
import random, string
key = "".join([random.SystemRandom().choice(string.digits + string.ascii_letters + "!#$%&()*+,-./:;<=>?@[]^_`{|}~") for i in range(60)])
print("SECRET_KEY = '%s'" % key)
```

Datei /var/www/django/frms/private/settings_local.py mit ähnlichem Inhalt an, Parameter anpassen, im Beispiel für die Installation an der HS Mittweida:
```python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from settings_private import *
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'saxid',
        'USER': 'saxid_rw',
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': 'postgresqql-host.hs-mittweida.de',
    },
}
# Der Realm vom EPPN der eigenen Nutzer
SERVER_REALM="hs-mittweida.de"
# Der URI unter dem die eigene Instanz erreichbar ist
HTTP_SERVER_NAME="https://saxid-api.hs-mittweida.de"
# Virtualhost unter dem die eigene Instanz erreichbar ist
ALLOWED_HOSTS = ["saxid-api.hs-mittweida.de"]
STATIC_URL  = '/static/api/'
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.RemoteUserBackend',
]
```





```bash
cd /var/www/django/frms

git clone https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms.git app

python3 -mvenv /var/www/django/frms/env
. /var/www/django/frms/env/bin/activate
cd /var/www/django/frms/app
pip install -r requirements.txt
./manage.py migrate

# Hier als Nutzername das angeben, was per Apache als REMOTE_USER rauskommt
./manage.py createsuperuser
./manage.py collectstatic
```

Dann noch in der Apache config (z.B. `/etc/apache2/sites-enabled/default-ssl.conf`) eintragen:

```
ProxyPass /api unix:/var/www/django/frms/uwsgi/uwsgi.sock|uwsgi://frms
# staticfiles
Alias /static/api /var/www/django/frms/static/
<Directory /var/www/django/frms/static/>
    Require all granted
    RequestHeader unset Cookie
    # Static Content ist 2 Tage gültig
    Header set Cache-Control "max-age=172800, public, must-revalidate"
    Options -Indexes -FollowSymLinks
    AllowMethods GET HEAD OPTIONS
    AllowOverride None
</Directory>

# Admin Oberfläche per htpasswd schützen
<Location "/api/admin">
  AuthType Basic
  AuthName "SaxID API"
  AuthUserFile /var/www/django/frms/private/htpasswd
  require valid-user
</Location>
# Alternativ Shibboleth:
# <Location "/api/admin">
#     AuthType shibboleth
#     ShibRequireSession On
#     Require user admin1
#     Require user admin2
# </Location>
```

Apache Konfiguration prüfen und neu starten:
~~~bash
apachectl configtest
# muss OK liefern

systemctl restart apache2

# Apache Basic Auth Nutzer anlegen (nur ohne Shibboleth)
# admin ist hier der Benutzername, der beim createsuperuser im Django angegeben wurde
htpasswd -c /var/www/django/frms/private/htpasswd admin
~~~


UWSGI einrichten
----------------

Konfigurationsdatei `/etc/uwsgi/apps-enabled/frms.ini` anlegen und dabei die Pythonversion bei `plugin` an die Distribution anpassen.

| Distribution | Version   |
| ------------ | --------- |
| Ubuntu 20.04 | python38  |
| Ubuntu 22.04 | python310 |
| Debian 10    | python37  |
| Debian 11    | python39  |

~~~
[virtualenv]
python = python3
[uwsgi]
# als der Linux-Nutzer läuft der Djangocode dann
uid = saxid
vassal_name = frms
#        v-------------- hier an Distribution anpassen
plugin = python38
disable-logging = false
processes = 2
cheaper = 1
threads = 2

socket = /var/www/django/frms/uwsgi/uwsgi.sock
chmod-socket = 660
virtualenv = /var/www/django/frms/env/
chdir = /var/www/django/frms/app/
wsgi-file = frms/wsgi.py
# Beim Anfassen des .uwsgi_reload-Files soll der uWSGI-Prozess neu gestartet werden.
touch-reload = /var/www/django/frms/.uwsgi_reload
processes = 4
cheaper = 2
threads = 2

; Umgeht Webserver-Fehler wie z.B. "invalid request block size"                 
buffer-size=65536

; Logging
plugin = logfile
; 2MB log (maxsize in bytes)
logger = file:logfile=/var/www/django/frms/logs/uwsgi.log,maxsize=2097152,backupname=/var/www/django/frms/logs/uwsgi.log.old
; Kein Request-Logging. Kann in der project.ini überschrieben werden            
disable-logging = true
~~~

~~~bash
systemctl stop uwsgi
systemctl enable uwsgi
systemctl start uwsgi
~~~

Jetzt muss die Weboberfläche erreichbar sein

Cronjobs und Dienste
====================

Es muß der Sync Dienst laufen:

```bash
    cp sync.service /etc/systemd/system/
    systemctl enable sync
    systemctl start sync
```

Wenn Daten per CSV importiert werden sollen, dann geht das per
```bash
/var/www/django/frms/env/bin/python /var/www/django/frms/app/manage.py sync_from_csv export.csv
```

Wichtig ist, dass dies als Nutzer `saxid` ausgeführt wird, da sonst die Permissions auf den Logfiles nicht stimmen.

Monitoring
==========
Im Monitoring kann der Zustand folgendermaßen überwacht werden:
```bash
/var/www/django/frms/env/bin/python /var/www/django/frms/app/manage.py check_health -v2
```
Return Code 0 heißt alles OK, Ungleich 0 heißt Fehler

Wichtig ist, dass der Monitoringjob als Benutzer `saxid` ausgeführt wird. Z.B. mit `runuser -u saxid /var/www/django/frms/env/bin/python /var/www/django/frms/app/manage.py check_health -v2`

Im Verzeichnis `monitoring/` gibt es Beispielscripte für xymon und checkmk.

Update
======
Wenn ein update ansteht, dann wäre folgendes zu tun:
```bash
. /var/www/django/frms/env/bin/activate
cd /var/www/django/frms/app
git pull
pip install -r requirements.txt
./manage.py collectstatic
touch frms/wsgi.py
```

Wenn Sie /api/admin nicht per Shibboleth schützen wollen, können Sie auch die Django Authentifizierung nutzen. Da sind dann ein paar kleine Änderungen in settings_local nötig.

Wenn alles funktioniert hat, müßten Sie unter `https://<fqdn>/api/res` eine Seite vom Django Rest Framework sehen.


Partner einrichten
==================

wenn das Django läuft, wäre noch folgendes zu tun:

Gehen Sie in den Django Admin und legen dort einen neuen Benutzer für die Synchronisation mit der Partnereinrichtung an mit folgenden Parametern:

Passwort ist egal, spielt eh keine Rolle, Auth geht per Token.
Benutzername: ist im Prinzip aber egal. In Chemnitz nutzen wir den FQDN der Partnerhochschule. Authentifizierung geht per Token
User type: sync user
REALM: den REALM vom EPPN der Partnerhochschule, z.B. tu-chemnitz.de
Api url: URL, unter der die API der Partnerhochschule erreichbar ist, z.B. https://saxid.hrz.tu-chemnitz.de/api/

Einer von beiden beteiligten Partnern muß das Token wechseln, beide benötigen dasselbe Token.
Dann noch das Token wechseln oder mitschickst mir das Token, das generiert wurde und ich ändere es. Es muß nur dasselbe Token sein.
```bash
./manage.py change_token
```

Bekannte API Instanzen in Sachsen:

* TU Chemnitz: https://saxid.hrz.tu-chemnitz.de/api/
* HS Mittweida: https://saxid-api.hs-mittweida.de/api/
* TU Freiberg: https://saxid.hrz.tu-freiberg.de/api/

Service Provider hinzufügen
===========================

siehe [separate Anleitung](new-service-provider.md)
